from fastapi import APIRouter
from models import Pydantic_Movies, Pydantic_Genre, Movies, Genre
from typing import List


router = APIRouter()


@router.get('/movies', response_model=List[Pydantic_Movies], tags=['movies'])
async def get_all_movies(genre: str = None):
    if genre:
        return await Pydantic_Movies.from_queryset(Movies.filter(genres__name=genre))
    else:
        return await Pydantic_Movies.from_queryset(Movies.all())


@router.get('/movies/{movie_id}', response_model=Pydantic_Movies, tags=['movies'])
async def get_single_movie(movie_id):
    return await Pydantic_Movies.from_queryset_single(Movies.get(pk=movie_id))


@router.get('/genres/all', response_model=List[Pydantic_Genre], tags=['genres'])
async def det_all_genres():
    return await Pydantic_Genre.from_queryset(Genre.all())
