from tortoise import fields, models, Tortoise
from tortoise.contrib.pydantic import pydantic_model_creator
from fastapi_users.db import TortoiseBaseUserModel, TortoiseUserDatabase
from fastapi_users.models import BaseUser, BaseUserCreate, BaseUserUpdate, BaseUserDB


class Genre(models.Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=20)

    class PydanticMeta:
        table = "Жанры"
        ordering = ['genre']

    def __str__(self):
        return f"{self.name}"


class Movies(models.Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=30)
    year = fields.IntField(default=2000)
    country = fields.TextField(max_length=100)
    genres = fields.ManyToManyField("models.Genre",
                                    related_name='movies')
    actors = fields.CharField(max_length=250)
    description = fields.TextField(max_length=500)
    is_active = fields.BooleanField(default=True)
    date_add = fields.DatetimeField(auto_now_add=True)

    class PydanticMeta:
        table = "Список фильмов"
        ordering = ['name']
        unique_together = ['name', 'author']
        exclude = ['is_active', 'date_add']

    def __str__(self):
        return f"{self.name}"


class User(BaseUser):
    pass


class UserCreate(BaseUserCreate):
    pass


class UserUpdate(User, BaseUserUpdate):
    pass


class UserDB(User, BaseUserDB):
    pass


class UserModel(TortoiseBaseUserModel):
    pass


Tortoise.init_models(['models'], 'models')
user_db = TortoiseUserDatabase(UserDB, UserModel)
Pydantic_Movies = pydantic_model_creator(Movies)
Pydantic_MoviesIn = pydantic_model_creator(Movies,
                                           name="MoviesIn",
                                           exclude_readonly=True)
Pydantic_Genre = pydantic_model_creator(Genre)
