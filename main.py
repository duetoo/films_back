from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from tortoise.contrib.fastapi import register_tortoise
from routers import router
from models import user_db, UserCreate, UserUpdate, User, UserDB
from fastapi_users.authentication import JWTAuthentication
from fastapi_users import FastAPIUsers


app = FastAPI(title="Simple movies app")


app.add_middleware(
    CORSMiddleware,
    allow_credentials=True,
    allow_origins=['*'],
    allow_methods=['*'],
    allow_headers=['*'],
)

SECRET = "SECRET"
jwt_authentication = JWTAuthentication(secret=SECRET, lifetime_seconds=3600)

fastapi_users = FastAPIUsers(
    user_db,
    [jwt_authentication],
    User,
    UserCreate,
    UserUpdate,

    UserDB,
)

app.include_router(router)
app.include_router(
    fastapi_users.get_auth_router(jwt_authentication),
    prefix="/auth/jwt",
    tags=["auth"],
)


def on_after_register(user: UserDB, request: Request):
    print(f"User {user.id} has registered.")


app.include_router(
    fastapi_users.get_register_router(on_after_register),
    prefix="/auth",
    tags=["auth"]
)

app.include_router(fastapi_users.get_users_router(),
                   prefix="/users",
                   tags=["users"])

register_tortoise(
    app,
    db_url="sqlite://db.sqlite3",
    modules={"models": ["models"]},
    generate_schemas=True,
    add_exception_handlers=True,
)

TORTOISE_ORM = {
        "connections": {
            "default": "sqlite://db.sqlite3"
            },
        "apps": {
            "models": {
                "models": ["models", "aerich.models"],
                "default_connection": "default",
            },
        }
    }
